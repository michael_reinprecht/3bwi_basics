package at.rei.basics.car;

public class Car {
	private String color;
	private String carname;
	private int maxspeed;
	private int price;
	private int fueluse;
	
	
	
	private Producer producer;
	
	private Engine engine;
	
	public Car(String color, int maxspeed, int price, int fueluse, Producer producer, Engine engine, String carname) {
		super();
		this.color = color;
		this.maxspeed = maxspeed;
		this.price = price;
		this.fueluse = fueluse;
		this.carname = carname;
		this.producer = producer;
		this.engine = engine;
	}
	

	public int getPrice() {
		return price;
	}


	public String getCarname() {
		return carname;
	}
	
	


	public int realPrice() {
		int realprice = (this.price * 100 - (this.price * producer.getDiscount()))/100;
		return realprice;
	}
	public double fueluse50p(int fueluse) {
		double fueluse50p = fueluse * 1.098;
		return fueluse50p;
	}
	
	public void PasteAll() {
		System.out.println("Data:\n"
	+ this.color + " : farbe\n"
	+ this.maxspeed + " : kmh\n"
	+ realPrice() + " : berechneter Preis\n"
	+ this.fueluse + " : Benzinverbrauch\n"
	+ this.producer.getName() + " : Name Anbieter\n"
	+ this.producer.getImmigration() + " : Migraionshintergrund\n"
	+ this.engine.getPower() + " : Motorstärke\n"
	+ this.engine.getType() + " : Motortyp" + "\n"
	+ fueluse50p(fueluse) + " : Benzinverbrauch nach 50000km");
	}
	
}
