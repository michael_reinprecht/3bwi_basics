package at.rei.basics.car;

public class CarMain {

	public static void main(String[] args) {
		Engine e1 = new Engine("Diesel", 600);
		Producer p1 = new Producer("Thomas", "kein Migrationshintergrund", 8);
		Car c1 = new Car("Rot", 160, 10000, 8, p1, e1, "Roari");
		Car c2 = new Car("Rot", 160, 10000, 8, p1, e1, "Feroari");
		Person pe1 = new Person("firstname", "lastname", "birthday");

		pe1.addCar(c1);
		pe1.addCar(c2);
		pe1.personInf();
	}

}
