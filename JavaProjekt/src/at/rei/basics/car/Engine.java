package at.rei.basics.car;

public class Engine {
	private String type;
	private int power;

	public Engine(String type, int power) {
		super();
		this.type = type;
		this.power = power;
	}

	public String getType() {
		return type;
	}

	public int getPower() {
		return power;
	}

}
