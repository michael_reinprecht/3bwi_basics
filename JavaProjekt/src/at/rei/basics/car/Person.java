package at.rei.basics.car;

import java.util.ArrayList;
import java.util.List;

public class Person {

	// TODO Auto-generated method stub
	private String firstname;
	private String lastname;
	private String birthday;

	private ArrayList<Car> cars;

	public Person(String firstname, String lastname, String birthday) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthday = birthday;
		this.cars = new ArrayList<>();
	}

	public void addCar(Car c) {
		this.cars.add(c);
	}

	public String getPersonsCarname() {
		String carnames = "";
		for (Car c : cars) {
			carnames = carnames + c.getCarname() + " ";
		}
		return carnames;
	}

	public int getValue() {
		int carvalue = 0;
		for (Car c : cars ) {
			carvalue += c.realPrice();
		}
		
		return carvalue;
	}
	

	public void personInf() {
		
		System.out.println(
		"Personal Data: \nBirthday: " 
		+ this.birthday + "\nFirstname: " 
		+ this.firstname + "\nLastname: " 
		+ this.lastname + "\nCars: " 
		+ this.getPersonsCarname() + "\nCar Value: "
		+ this.getValue());
		
	}

}
