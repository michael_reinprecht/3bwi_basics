package at.rei.basics.car;

public class Producer {
	private String name;
	private String immigration;
	private int discount;

	public Producer(String name, String immigration, int discount) {
		super();
		this.name = name;
		this.immigration = immigration;
		this.discount = discount;
	}

	public String getName() {
		return name;
	}

	public String getImmigration() {
		return immigration;
	}

	public int getDiscount() {
		return discount;
	}

}
