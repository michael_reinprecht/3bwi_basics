package at.rei.basics.laptop;

public class Battery {

	private int chargingstatus;
	private String type;

	public Battery(int chargingstatus, String type) {
		super();
		setChargingstatus(chargingstatus);
		
		this.type = type;
	}

	public int getChargingstatus() {
		
		return chargingstatus;
	}

	public void setChargingstatus(int chargingstatus) {
		this.chargingstatus = chargingstatus;
		if (chargingstatus > 100) {
			this.chargingstatus = 100;
		}
	}

	public String getType() {
		return type;
	}

}
