package at.rei.basics.laptop;

public class Laptop {
	private double weight;
	private double height;
	private double zoll;
	private boolean isOn = false;
	
	private Battery battery;
	
	

	public Laptop(double w, double h, double z, Battery battery) {
		super();
		this.weight = w;
		this.height = h;
		this.zoll = z;
		
		this.battery = battery;
	}


	public void turnOn() {
		this.isOn = true;
	}

	public void turnOff() {
		this.isOn = false;
	}

	public void pasteData() {
		System.out.println("This is my Data: \n\n Weight:" + this.weight + 
				"\n Height: " + this.height +
				"\n Screen: " + this.zoll +
				"\n Turned on: " +
				this.isOn + "\n\n" +
				"\n Battery Status: " + battery.getChargingstatus());
	}
}