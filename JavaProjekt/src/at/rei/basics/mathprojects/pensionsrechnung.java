package at.rei.basics.mathprojects;

public class pensionsrechnung {
		private static double startgeld = 10000;
		private static int zeit = 45;
		private static int differenzZuRente = 10;
		private static double einzahlRente = 1200;
		private static double zinssatz = 1.25;
		private static double änderungswert;
		private static float ergebnis;
		
		private static int auszahlRente = 2000;
		private static int Rentendauer;
		
		private static int neueAuszahlRente = 1000;
		private static int neueRentendauer;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		änderungswert = 1 + (zinssatz/100);
		ergebnis = (float) ((startgeld * Math.pow(änderungswert, zeit)) + einzahlRente * (((Math.pow(änderungswert, zeit-differenzZuRente)-1)/(änderungswert-1))*änderungswert));
		
		Rentendauer = (int) (Math.log(-auszahlRente/(ergebnis * änderungswert - ergebnis - auszahlRente))/(Math.log(änderungswert)));
		neueRentendauer = (int) (Math.log(-neueAuszahlRente/(ergebnis * änderungswert - ergebnis - neueAuszahlRente))/(Math.log(änderungswert)));
		System.out.println
		("Nach " + zeit + " Jahren hat Charly " + ergebnis + "€ angespart."
				+ "\nBei einer Rentenauszahlung von " + auszahlRente + "€"
						+ " kann diese eine Rentendauer von " + Rentendauer + " Jahren haben."
								+ "\nSollte die Rentenauszahlung auf " + neueAuszahlRente + "€ geändert werden,"
										+ " so ändert sich die Rentendauer auf " + neueRentendauer + " Jahre.");
	}

}
